package umlmaker.GUI.Components;

import javax.swing.Icon;
import javax.swing.JLabel;
import umlmaker.ClassData;

/**
 *
 * @author Oliver
 */
public class JClassLabel extends JLabel {

	private static final long serialVersionUID = 1L;

	private final ClassData classData;

	public JClassLabel(ClassData classData) {
//		super(classData.getName());
		super(classData.toString());
		this.classData = classData;
	}

	public JClassLabel(Icon image, ClassData classData) {
		super(image);
		setText(classData.getClassName());
		this.classData = classData;
	}

	public JClassLabel(Icon image, int horizontalAlignment, ClassData classData) {
		super(image, horizontalAlignment);
		setText(classData.getClassName());
		this.classData = classData;
	}

	public JClassLabel(String text, ClassData classData) {
		super(text);
		this.classData = classData;
	}

	public JClassLabel(String text, int horizontalAlignment, ClassData classData) {
		super(text, horizontalAlignment);
		this.classData = classData;
	}

	public JClassLabel(String text, Icon icon, int horizontalAlignment, ClassData classData) {
		super(text, icon, horizontalAlignment);
		this.classData = classData;
	}

	public ClassData getClassData() {
		return classData;
	}

	public String getClassName() {
		return classData.getClassName();
	}

}
