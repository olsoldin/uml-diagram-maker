package umlmaker.GUI.Components;

import umlmaker.ClassData;
import umlmaker.Tokens.Attribute;
import umlmaker.Tokens.Method;

import javax.swing.*;
import java.awt.*;
import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;

public class ClassDataBox {
	private JLabel lblClassName;
	private JPanel pnlClassTitle;
	private JPanel pnlClassAttributes;
	private JPanel pnlClassMethods;
	private ClassData classData;
	private JList lstClassAttributes;
	private JList lstClassMethods;
	private JPanel pnlMain;
	private static int zOrderMax = 0;
	private int zOrder;


	public ClassDataBox(ClassData classData) {
		this.classData = classData;
		setClassName(classData.getClassName());
		setAttributes(classData.getAttributes());
		setMethods(classData.getMethods());
		this.zOrder = zOrderMax++;
	}

	public ClassData getClassData() {
		return classData;
	}

	public JPanel get() {
		/*
		 * Make the panel the smallest size that still shows all the contents
		 */
		Dimension preferredSize = pnlMain.getPreferredSize();
		preferredSize.setSize(preferredSize.getWidth(), preferredSize.getHeight());
		pnlMain.setPreferredSize(preferredSize);
		pnlMain.setSize(preferredSize);

		pnlMain.setVisible(true);

		// TODO: Make this work better so they don't all overlap
		pnlMain.setLocation(100, 100);

		// TODO: Convert to JLayeredPane
		return pnlMain;
	}

	public JPanel getPnl() {
		return pnlMain;
	}

	public ClassDataBox setClassName(String className) {
		this.lblClassName.setText(className);
		return this;
	}
	public ClassDataBox setAttributes(List<Attribute> attributes) {
		List<String> listData = attributes.stream().map(Attribute::toString).collect(Collectors.toList());
		lstClassAttributes.setListData(new Vector<>(listData));
		return this;
	}

	public ClassDataBox setMethods(List<Method> methods) {
		List<String> listData = methods.stream().map(Method::toString).collect(Collectors.toList());
		lstClassMethods.setListData(new Vector<>(listData));
		return this;
	}

	public int getX() {
		return pnlMain.getX();
	}

	public int getY() {
		return pnlMain.getY();
	}

	public int getWidth() {
		return pnlMain.getWidth();
	}

	public int getHeight() {
		return pnlMain.getHeight();
	}

	public void setLocation(int x, int y) {
		pnlMain.setLocation(x, y);
	}

	public void setVisible(boolean aFlag) {
		pnlMain.setVisible(aFlag);
	}

	public int getZOrder() {
		return this.zOrder;
	}

	public void setZOrder(int zOrder) {
		this.zOrder = zOrder;
	}

	public int incrementZOrder() {
		this.zOrder++;
		if (this.zOrder > zOrderMax) {
			zOrderMax = this.zOrder;
		}
		return this.zOrder;
	}
}
