/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package umlmaker.GUI;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.filechooser.FileFilter;

import umlmaker.ClassData;
import umlmaker.ClassRelationship;
import umlmaker.GUI.Components.ClassDataBox;
import umlmaker.Tokens.*;
import umlmaker.UMLMaker;

/**
 * @author Oliver
 */
public class UMLGUI extends JFrame {

    private final Settings settings;

	private UMLMaker uml;
	private List<ClassDataBox> boxes = new ArrayList<>();


    private final JButton btnGenerate = new JButton();
    private final JButton btnClear = new JButton();
    private final JPanel panelButtons = new JPanel();
    private final JPanel panelUML = new JPanel();
	private final JLayeredPane layers = new JLayeredPane();
    private final JFileChooser fileChooser = new JFileChooser();
    private final JLabel lblCurrentPath = new JLabel();
    private final JPanel panelSidebar = new JPanel();
    private final JMenuBar menuBar = new JMenuBar();
    private final JMenu menuFile = new JMenu();
    private final JMenuItem menuItemChooseFolder = new JMenuItem();
    private final JMenu menuEdit = new JMenu();
    private final JMenu menuEditLAF = new JMenu();
    private final JList<String> classList = new JList<>();


	/**
	 * Creates new form UMLGUI
	 */
    public UMLGUI(Settings settings) {
        this.settings = settings;

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		initComponents();
        setLookAndFeel(settings);
	}


	/**
	 * This method is called from within the constructor to
	 * initialize the form.
	 */
	private void initComponents() {
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        // Set up the menu bar
        initMenuBar();
        setJMenuBar(menuBar);

        // Add some placeholder text
		lblCurrentPath.setText("Please choose a folder from the File -> Choose Folder option");
		getContentPane().add(lblCurrentPath, BorderLayout.PAGE_START);

        // Add the buttons
        initPanelButtons();
        getContentPane().add(panelButtons, BorderLayout.PAGE_END);

        // Add the main panel
        initPanelMain();
        getContentPane().add(panelUML, BorderLayout.CENTER);

        // Set up the sidebar
        initPanelSidebar();
        getContentPane().add(panelSidebar, BorderLayout.WEST);


        pack();

        // Set up the file chooser
        String lastDirectory = settings.getLastDirectory();

        fileChooser.setFileFilter(new FolderFilter());
        fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        fileChooser.setCurrentDirectory(new File(lastDirectory));
        panelUML.addMouseMotionListener(new MyMouseMotionListener());
        panelUML.addMouseListener(new MyMouseListener());
		panelUML.add(layers);
    }

    private void initMenuBar() {
        menuFile.setText("File");
        {
            menuItemChooseFolder.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.CTRL_DOWN_MASK));
            menuItemChooseFolder.setText("Choose Folder");
            menuItemChooseFolder.addActionListener(this::menuItemChooseFolderActionPerformed);
            menuFile.add(menuItemChooseFolder);
        }
        menuBar.add(menuFile);

        menuEdit.setText("Edit");
        {
            menuEditLAF.setText("Choose Look and Feel");
            for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                JMenuItem laf = new JMenuItem();
                laf.setText(info.getName());
                laf.addActionListener(this::menuItemChooseLAFActionPerformed);
                menuEditLAF.add(laf);
            }
            menuEdit.add(menuEditLAF);
        }
        menuBar.add(menuEdit);
    }

    private void initPanelButtons() {
        panelButtons.setLayout(new GridLayout(1, 0));

		btnGenerate.setText("Generate");
		btnGenerate.setEnabled(false);
		btnGenerate.addActionListener(this::btnGenerateActionPerformed);
        panelButtons.add(btnGenerate);

		btnClear.setText("Clear");
		btnClear.addActionListener(this::btnClearActionPerformed);
        panelButtons.add(btnClear);
    }

    private void initPanelMain() {
		GroupLayout panelUMLLayout = new GroupLayout(panelUML);
		panelUMLLayout.setHorizontalGroup(
				panelUMLLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
						.addGap(0, 420, Short.MAX_VALUE)
		);
		panelUMLLayout.setVerticalGroup(
				panelUMLLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
						.addGap(0, 242, Short.MAX_VALUE)
		);
		layers.setLayout(panelUMLLayout);
//        panelUML.setLayout(panelUMLLayout);
    }

    private void initPanelSidebar() {
        classList.getSelectionModel().addListSelectionListener(e -> {
            ListSelectionModel lsm = (ListSelectionModel) e.getSource();
            // Find out which indexes are selected.
            int minIndex = lsm.getMinSelectionIndex();
            int maxIndex = lsm.getMaxSelectionIndex();
            for (int i = minIndex; i <= maxIndex; i++) {
                if (lsm.isSelectedIndex(i) && !lsm.getValueIsAdjusting()) {
                    // Because the boxes are created from the exact same list as the classlist, all the indexes are the same
                    ClassDataBox box = boxes.get(i);
                    // TODO: Make this bring the selected box to the front
                    box.get().setLocation(0, 0);
                }
            }
        });
        panelSidebar.add(classList, BorderLayout.PAGE_START);
    }

    private void updatePanelSidebar() {
        classList.setListData(new Vector<>(uml.getClassData().keySet()));
	}


	private void menuItemChooseFolderActionPerformed(ActionEvent evt) {
		int option = fileChooser.showOpenDialog(this);
		if (option == JFileChooser.APPROVE_OPTION) {
            uml = new UMLMaker(fileChooser.getSelectedFile().getPath());
			lblCurrentPath.setText(uml.getPath());
            settings.setLastDirectory(fileChooser.getCurrentDirectory().getPath());
			btnGenerate.setEnabled(true);
		}
	}

    private void menuItemChooseLAFActionPerformed(ActionEvent evt) {
        setLookAndFeel(settings, evt.getActionCommand());
    }

	private void btnGenerateActionPerformed(ActionEvent evt) {
        uml.generate();
		clear();
		uml.getClassData().values().forEach(this::createClassDataBox);
		uml.getRelationships().forEach(this::createLink);
        updatePanelSidebar();
		repaint();
	}

	private void btnClearActionPerformed(ActionEvent evt) {
		clear();
	}


	private void createClassDataBox(ClassData cd) {
		ClassDataBox cdb = new ClassDataBox(cd);
//		panelUML.add(cdb.get());
		layers.add(cdb.get());
		boxes.add(cdb);
	}


	private void createLink(ClassRelationship rel) {
		Point2D from = new Point2D(0, 0);
		Point2D to = new Point2D(0, 0);
		for (ClassDataBox box : boxes) {
			ClassData labelClass = box.getClassData();
			if (rel.from() == labelClass) {
				from.setX(box.getX());
				from.setY(box.getY());
			} else if (rel.to() == labelClass) {
				to.setX(box.getX());
				to.setY(box.getY());
			}
		}


		drawArrow(from, to, rel.getRelationship());
	}


	private void redrawArrow(ClassDataBox toBox) {
		ClassRelationship rel = uml.getRelationshipTo(toBox.getClassData());
		if (rel == null || rel.containsNull()) {
			return;
		}

		Point2D from = new Point2D();
		Point2D to = new Point2D();

		for (ClassDataBox fromBox : boxes) {
			if (fromBox.getClassData() == rel.from()) {
				from.setX(fromBox.getX() + fromBox.getWidth() / 2);
				from.setY(fromBox.getY() + fromBox.getHeight() / 2);
				to.setX(to.getX() + toBox.getWidth() / 2);
				to.setY(to.getY() + toBox.getHeight() / 2);
				drawArrow(from, to, rel.getRelationship());
				return;
			}
		}
	}


	private void drawArrow(Point2D from, Point2D to, Relationship rel) {
		Color c = null;
//		Color c = switch (rel) {
//			case USES -> Color.BLACK;
//			case EXTENDS -> Color.BLUE;
//			case IMPLEMENTS -> Color.RED;
//		};
		panelUML.getGraphics().setColor(c);
		panelUML.getGraphics().drawLine(from.getX(), from.getY(), to.getX(), to.getY());
		panelUML.getGraphics().drawRect(to.getX(), to.getY(), 10, 10);
		//@TODO: different arrows for different relationships
	}


	private void clear() {
		boxes.forEach((box) -> {
//			panelUML.remove(box.get());
			layers.remove(box.get());
			box.setVisible(false);
		});
		boxes = new ArrayList<>();
        panelButtons.revalidate();
		repaint();
	}


	@Override
	public void repaint() {
		super.repaint();
	}


	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {
        // Load preferences
        Settings settings = Settings.getSettings("umlmaker.GUI.preferences");

        /* Create and display the form */
        EventQueue.invokeLater(() -> new UMLGUI(settings).setVisible(true));
    }

    private void setLookAndFeel(Settings settings) {
        setLookAndFeel(settings, settings.getLookAndFeel());
    }

    private void setLookAndFeel(Settings settings, String laf) {
		try {
            for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if (info.getName().equals(laf)) {
                    // Set the LAF and save it to the preferences
                    UIManager.setLookAndFeel(info.getClassName());
                    settings.setLookAndFeel(info.getName());
                    // Force both the main window and the file chooser to update
                    SwingUtilities.updateComponentTreeUI(this);
                    fileChooser.updateUI();
                    this.pack();
					break;
				}
			}
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
            Logger.getLogger(UMLGUI.class.getName()).log(Level.SEVERE, null, e);
		}
	}


	private static class FolderFilter extends FileFilter {

		@Override
		public boolean accept(File file) {
			return file.isDirectory();
		}


		@Override
		public String getDescription() {
			return "Folders only";
		}
	}


	private int xOffset, yOffset, labelNo;
	private boolean mouseOverBox = false;

	/**
	 * The offset is set by the mouse move and the position is set by the mouse dragged
	 * this is because when both are in the same method, there is an inconsistent bug
	 * where the box is randomly dropped, meaning you have to click it again to move it
	 * I think it's related to the mouse being moved fast enough to exit the box before
	 * the code is run, so when the listener is called the mouse isn't in the box
	 * anymore, so splitting it so the offset is calculated as the mouse moves stops the
	 * bug completely
	 */
	private class MyMouseMotionListener implements MouseMotionListener {

		@Override
		public void mouseDragged(MouseEvent e) {
			ClassDataBox box = boxes.get(labelNo);
			int x = e.getX(), y = e.getY();

			if (mouseOverBox) {
				box.setLocation(x - xOffset, y - yOffset);
//				panelUML.repaint();
				layers.repaint();
				boxes.forEach(UMLGUI.this::redrawArrow);
			}
		}


		@Override
		public void mouseMoved(MouseEvent e) {
			boxes.forEach(UMLGUI.this::redrawArrow);
			int x = e.getX(), y = e.getY();
			mouseOverBox = false;

			// Reverse loop to select the box at the top
			// i.e. the one added last
			for (int i = boxes.size() - 1; i >= 0; i--) {
				ClassDataBox box = boxes.get(i);
				//if the mouse is inside the box
				if (x >= box.getX() && x <= box.getX() + box.getWidth()
						&& y >= box.getY() && y <= box.getY() + box.getHeight()) {
					xOffset = x - box.getX();
					yOffset = y - box.getY();
					labelNo = i;
					mouseOverBox = true;
				}
			}
		}
	}

	private class MyMouseListener implements MouseListener {

		@Override
		public void mouseClicked(MouseEvent e) {

		}

		@Override
		public void mousePressed(MouseEvent e) {
//			ClassDataBox box = boxes.get(labelNo);

            // TODO: This but working
			if (mouseOverBox) {
//                panelUML.setComponentZOrder(box.get(), -1);
//				panelUML.repaint();
				layers.repaint();
				boxes.forEach(UMLGUI.this::redrawArrow);
                classList.setSelectedIndex(labelNo);
			}
		}

		@Override
		public void mouseReleased(MouseEvent e) {

		}

		@Override
		public void mouseEntered(MouseEvent e) {

		}

		@Override
		public void mouseExited(MouseEvent e) {

		}
	}
}
