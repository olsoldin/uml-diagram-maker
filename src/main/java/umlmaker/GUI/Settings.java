package umlmaker.GUI;

import java.util.HashMap;
import java.util.Map;
import java.util.prefs.Preferences;

public class Settings {
    private static final Map<String, Settings> settingsForPaths = new HashMap<>();

    private static final String PREF_LAST_DIRECTORY = "lastDirectory";
    private static final String DEF_LAST_DIRECTORY = "";

    private static final String PREF_LOOK_AND_FEEL = "lookAndFeel";
    private static final String DEF_LOOK_AND_FEEL = "Metal";

    private final Preferences preferences;



    private Settings(String pathName) {
        preferences = Preferences.userRoot().node(pathName);
    }


    public String getLastDirectory() {
        return getLastDirectory(DEF_LAST_DIRECTORY);
    }

    public String getLastDirectory(String def) {
        return preferences.get(PREF_LAST_DIRECTORY, def);
    }

    public void setLastDirectory(String lastDirectory) {
        preferences.put(PREF_LAST_DIRECTORY, lastDirectory);
    }


    public String getLookAndFeel() {
        return getLookAndFeel(DEF_LOOK_AND_FEEL);
    }

    public String getLookAndFeel(String def) {
        return preferences.get(PREF_LOOK_AND_FEEL, def);
    }

    public void setLookAndFeel(String laf) {
        preferences.put(PREF_LOOK_AND_FEEL, laf);
    }





    public static Settings getSettings(String pathName) {
        Settings settingsForPath = settingsForPaths.get(pathName);
        if (settingsForPath == null) {
            settingsForPaths.put(pathName, new Settings(pathName));
            return getSettings(pathName);
        }
        return settingsForPath;
    }
}
