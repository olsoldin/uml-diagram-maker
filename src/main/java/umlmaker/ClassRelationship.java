package umlmaker;

import umlmaker.Tokens.*;

/**
 *
 * @author Oliver
 */
public class ClassRelationship{

	private ClassData from;
	private ClassData to;
	private Relationship relationship;


	public ClassRelationship(ClassData from, ClassData to, Relationship relationship){
		this.from = from;
		this.to = to;
		this.relationship = relationship;
	}


	public void setTo(ClassData to){
		this.to = to;
	}


	public void setFrom(ClassData from){
		this.from = from;
	}


	public void setRelationship(Relationship rel){
		this.relationship = rel;
	}


	public boolean containsNull(){
		return from == null || to == null || relationship == null;
	}


	public ClassData from(){
		return from;
	}


	public ClassData to(){
		return to;
	}


	public Relationship getRelationship(){
		return relationship;
	}


	public String getRelationshipString(){
		return relationship.toString();
	}


	@Override
	public String toString(){
		return from.getClassName() + " -> " + (to == null ? "external" : to.getClassName()) + " = " + relationship;
	}
}
