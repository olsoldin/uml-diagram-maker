/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package umlmaker;

import org.jboss.forge.roaster.Roaster;
import org.jboss.forge.roaster.model.source.*;
import umlmaker.Tokens.*;

import java.io.*;
import java.util.*;

/**
 *
 * @author Oliver
 */
public class UMLMaker{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args){
        UMLMaker uml;
        if(args.length > 0){
            uml = new UMLMaker(args[0]);
        }else{
//            uml = new UMLMaker();
//			uml = new UMLMaker("D:\\Documents\\Programming\\NetBeansProjects\\UHNetSim");
//            uml = new UMLMaker("D:\\Documents\\Programming\\Java Projects\\uml-diagram-maker");
            uml = new UMLMaker("/home/oliver/Documents/Progamming/Java/uml-diagram-maker/");
        }
        uml.generate();
    }

    private String folderPath = ".";
    private final List<File> javaFiles = new ArrayList<>(); //All the files to check
    private final List<String> classes = new ArrayList<>(); //The name of all the classes
    private final Set<ClassRelationship> relationships = new HashSet<>(); //The relationships between the classes
    private final Map<String, ClassData> classData = new HashMap<>(); // Processed class data, useful for the UML data
    private final Map<String, JavaSource> classSourceData = new HashMap<>(); // Raw parsed java file


    /**
     * Creates a new UMLMaker in the folder specified
     * <p>
     * @param folderPath the path to get all the .java files from
     */
    public UMLMaker(String folderPath){
        this.folderPath = folderPath;
    }


    /**
     * A default constructor, setting the path
     * the jar file was run from
     */
    public UMLMaker(){
    }


    /**
     * Finds all the classes in the directory, then finds all the relationships
     * between them and stores all data internally ready for retrieval
     */
    public void generate(){
        if(classes.isEmpty()){
            findClasses();

            //Find all the relationships between all of those files
            classSourceData.values().forEach(this::findRelationships);
        }

    }


    public Set<ClassRelationship> getRelationships(){
        return relationships;
    }
    
    public ClassRelationship getRelationshipTo(ClassData cd){
        for(ClassRelationship rel : relationships){
            if(rel.to() == cd){
                return rel;
            }
        }
        return null;
    }


    public Map<String, ClassData> getClassData(){
        return classData;
    }


    /**
     * Finds all the classes in the directory and adds them to an arraylist
     */
    private void findClasses(){
        search(folderPath);
        javaFiles.forEach((File f) -> {
            try{
                JavaSource javaClass = Roaster.parse(JavaSource.class, f);
                String className = javaClass.getCanonicalName();

                classes.add(className);
                classData.put(className, getClassData(javaClass));
                classSourceData.put(className, javaClass);
            }catch(IOException ignored){
            }
        });
    }


    /**
     * Searches the folder for all .java files
     * any folders are also searched
     * all .java files are added to the arraylist
     * <b>javaFiles</b>
     * <p>
     * @param folderPath the path of the folder to search in
     */
    private void search(String folderPath){
        //Create a new folder with the current path
        File folder = new File(folderPath);
        //Add all the files in the directory ending in .java
        File[] files = folder.listFiles((File pathname) -> pathname.getName().matches(".*\\.java"));

        if (files == null) {
            System.err.println("No Java files found.");
            return;
        }

        javaFiles.addAll(Arrays.asList(files));

        //Find all the folders in the current folder
        File[] folders = folder.listFiles(File::isDirectory);

        if (folders == null) {
            return;
        }

        //Recursively search each folder in order
        for(File f : folders){
            search(f.getPath());
        }
    }



    private void findRelationships(JavaSource javaClass) {
        // TODO: Remember to fill this out for enums / interfaces etc.!!
        String className = javaClass.getCanonicalName();
        ClassData from = classData.get(className);

        if (javaClass.isClass()) {
            JavaClassSource javaClassSource = (JavaClassSource) javaClass;
            String superType = javaClassSource.getSuperType();
            List<String> interfaces = javaClassSource.getInterfaces();


            // Extends
            if (!"java.lang.Object".equals(superType)) {
                ClassData to = classData.get(superType);
                relationships.add(new ClassRelationship(from, to, Relationship.EXTENDS));
            }

            // Implements
            interfaces.forEach(iface -> {
                ClassData to = classData.get(iface);
                relationships.add(new ClassRelationship(from, to, Relationship.IMPLEMENTS));
            });

            // Uses
            from.getAttributes().forEach(attr -> {
                ClassData to = classData.get(attr.getArgument().getType());
                ClassRelationship rel = new ClassRelationship(from, to, Relationship.USES);
                relationships.add(rel);
            });
            from.getMethods().forEach(method -> {
                ClassData to = classData.get(method.getMethodSignature().getType());
                ClassRelationship rel = new ClassRelationship(from, to, Relationship.USES);

                // We don't care if we are using external classes
                // e.g. java.util.List
                if (!rel.containsNull()) {
                    relationships.add(rel);
                }

                method.getArguments().forEach(arg -> {
                    ClassData toArg = classData.get(arg.getType());
                    ClassRelationship relArg = new ClassRelationship(from, toArg, Relationship.USES);

                    if (!relArg.containsNull()) {
                        relationships.add(relArg);
                    }

                });
            });
        }
    }


    /**
     * @param javaClass
     *          <p>
     * @return
     *         <p>
     */
    private ClassData getClassData(JavaSource javaClass) {
        ClassDataBuilder cdb = new ClassDataBuilder();

        cdb.addClassName(javaClass.getName());
        cdb.addClassType(getClassType(javaClass));
        cdb.addVisibility(convertToUmlVisibility(javaClass.getVisibility()));
        cdb.addAttributes(getAttributes(javaClass));
        cdb.addMethods(getMethods(javaClass));

        return cdb.build();
    }

    private Visibility convertToUmlVisibility(org.jboss.forge.roaster.model.Visibility vis) {
        if (vis.scope().length() == 0) {
            return Visibility.NONE;
        }
        return Visibility.valueOf(vis.scope().toUpperCase());
    }

    private ClassType getClassType(JavaSource javaClass) {
        if (javaClass.isClass()) {
            return ClassType.CLASS;
        }
        if (javaClass.isEnum()) {
            return ClassType.ENUM;
        }
        if (javaClass.isInterface()) {
            return ClassType.INTERFACE;
        }
        if (javaClass.isAnnotation()) {
            return ClassType.ANNOTATION;
        }
        if (javaClass.isRecord()) {
            return ClassType.RECORD;
        }
        return null;
    }

    private List<Attribute> getAttributes(JavaSource javaClass) {
        if (javaClass.isClass()) {
            JavaClassSource javaClassSource = (JavaClassSource) javaClass;
            List<FieldSource<JavaClassSource>> fields = javaClassSource.getFields();
            List<Attribute> attributes = new ArrayList<>(fields.size());
            for (FieldSource<JavaClassSource> field : fields) {
                Argument arg = new Argument();
                arg.setName(field.getName());
                arg.setType(field.getType().getName());
                arg.setFinal(field.isFinal());
                Visibility _vis = convertToUmlVisibility(field.getVisibility());
                attributes.add(new Attribute(arg, _vis));
            }
            return attributes;
        }
        if (javaClass.isEnum()) {
            JavaEnumSource javaEnumSource = (JavaEnumSource) javaClass;
            List<FieldSource<JavaEnumSource>> fields = javaEnumSource.getFields();
            List<Attribute> attributes = new ArrayList<>(fields.size());
            for (FieldSource<JavaEnumSource> field : fields) {
                Argument arg = new Argument();
                arg.setName(field.getName());
                arg.setType(field.getType().getName());
                arg.setFinal(field.isFinal());
                Visibility _vis = convertToUmlVisibility(field.getVisibility());
                attributes.add(new Attribute(arg, _vis));
            }
            return attributes;
        }
        if (javaClass.isInterface()) {
            JavaInterfaceSource javaInterfaceSource = (JavaInterfaceSource) javaClass;
            List<FieldSource<JavaInterfaceSource>> fields = javaInterfaceSource.getFields();
            List<Attribute> attributes = new ArrayList<>(fields.size());
            for (FieldSource<JavaInterfaceSource> field : fields) {
                Argument arg = new Argument();
                arg.setName(field.getName());
                arg.setType(field.getType().getName());
                arg.setFinal(field.isFinal());
                Visibility _vis = convertToUmlVisibility(field.getVisibility());
                attributes.add(new Attribute(arg, _vis));
            }
            return attributes;
        }
        if (javaClass.isAnnotation()) {
            // TODO: Handle this
        }
        if (javaClass.isRecord()) {
            // TODO: Handle this
        }

        return new ArrayList<>();
    }

    private List<Method> getMethods(JavaSource javaClass) {
        if (javaClass.isClass()) {
            List<MethodSource<JavaClassSource>> javaClassMethods = ((JavaClassSource)javaClass).getMethods();
            List<Method> methods = new ArrayList<>(javaClassMethods.size());
            for (MethodSource<JavaClassSource> method : javaClassMethods) {
                Argument methodSignature = new Argument();
                methodSignature.setName(method.getName());
                if (!method.isReturnTypeVoid()) {
                    methodSignature.setType(method.getReturnType().getName());
                } else {
                    methodSignature.setType("void");
                }
                Visibility _vis = convertToUmlVisibility(method.getVisibility());
                List<Argument> arguments = new ArrayList<>();
                for (int i = 0; i < method.getParameters().size(); i++) {
                    ParameterSource parameter = method.getParameters().get(i);
                    Argument arg = new Argument();
                    arg.setName(parameter.getName());
                    arg.setType(parameter.getType().getName());
                    arguments.add(arg);
                }
                methods.add(new Method(methodSignature, _vis, arguments));
            }
            return methods;
        }
        if (javaClass.isEnum()) {
            List<MethodSource<JavaEnumSource>> javaEnumMethods = ((JavaEnumSource)javaClass).getMethods();
            List<Method> methods = new ArrayList<>(javaEnumMethods.size());
            for (MethodSource<JavaEnumSource> method : javaEnumMethods) {
                Argument methodSignature = new Argument();
                methodSignature.setName(method.getName());
                if (!method.isReturnTypeVoid()) {
                    methodSignature.setType(method.getReturnType().getName());
                } else {
                    methodSignature.setType("void");
                }
                Visibility _vis = convertToUmlVisibility(method.getVisibility());
                List<Argument> arguments = new ArrayList<>();
                for (int i = 0; i < method.getParameters().size(); i++) {
                    ParameterSource parameter = method.getParameters().get(i);
                    Argument arg = new Argument();
                    arg.setName(parameter.getName());
                    arg.setType(parameter.getType().getName());
                    arguments.add(arg);
                }
                methods.add(new Method(methodSignature, _vis, arguments));
            }
            return methods;
        }
        if (javaClass.isInterface()) {
            List<MethodSource<JavaInterfaceSource>> javaInterfaceMethods = ((JavaInterfaceSource)javaClass).getMethods();
            List<Method> methods = new ArrayList<>(javaInterfaceMethods.size());
            for (MethodSource<JavaInterfaceSource> method : javaInterfaceMethods) {
                Argument methodSignature = new Argument();
                methodSignature.setName(method.getName());
                if (!method.isReturnTypeVoid()) {
                    methodSignature.setType(method.getReturnType().getName());
                } else {
                    methodSignature.setType("void");
                }
                Visibility _vis = convertToUmlVisibility(method.getVisibility());
                List<Argument> arguments = new ArrayList<>();
                for (int i = 0; i < method.getParameters().size(); i++) {
                    ParameterSource parameter = method.getParameters().get(i);
                    Argument arg = new Argument();
                    arg.setName(parameter.getName());
                    arg.setType(parameter.getType().getName());
                    arguments.add(arg);
                }
                methods.add(new Method(methodSignature, _vis, arguments));
            }
            return methods;
        }
        if (javaClass.isAnnotation()) {
            // TODO: Handle this
        }
        if (javaClass.isRecord()) {
            // TODO: Handle this
        }
        return new ArrayList<>();
    }


    private String getFileContents(File f) throws IOException{
        BufferedInputStream ins = new BufferedInputStream(new FileInputStream(f));

        byte[] buff = new byte[2048];
        StringBuilder sb = new StringBuilder();

        while(true){
            int rc = ins.read(buff);
            if(rc <= 0){
                break;
            }

            sb.append(new String(buff, 0, rc));
        }
        return sb.toString();
    }


    public String getPath(){
        return folderPath;
    }

}
