package umlmaker.TestPackage;

import umlmaker.ClassData;
import umlmaker.Tokens.Attribute;
import umlmaker.Tokens.ClassType;
import umlmaker.Tokens.Method;
import umlmaker.Tokens.Visibility;

import java.util.List;

/**
 *
 * @author Oliver
 */
public abstract class test extends ClassData {

	public static void main(String[] args) {

	}

	public test(String className, ClassType classType, Visibility visibility, List<Attribute> attributes, List<Method> methods) {
		super(className,
				classType,
				visibility,
				attributes,
				methods);
	}
}
