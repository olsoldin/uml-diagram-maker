package umlmaker.javaparser;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.google.common.base.Strings;
import umlmaker.javaparser.explorer.DirExplorer;
import umlmaker.javaparser.tokens.ClassToken;
import umlmaker.javaparser.visitor.ClassVisitor;
import umlmaker.javaparser.visitor.EnumVisitor;
import umlmaker.javaparser.visitor.FieldVisitor;
import umlmaker.javaparser.visitor.MethodVisitor;


import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class JavaParserTest {
    public static void main(String... args) {
        File projectDir = new File("I:\\Documents\\Programming\\Java Projects\\uml-diagram-maker\\src\\");
        JavaParserTest test = new JavaParserTest();

        test.go(projectDir);
    }



    public JavaParserTest() {

    }

    public void go(File root) {
        DirExplorer exp = new DirExplorer((level, path, file) -> path.endsWith(".java") && path.contains("Implements"),
                (level, path, file) -> {
                    System.out.println("\n");
                    System.out.println(path);
                    System.out.println(Strings.repeat("#", path.length()));
                    parseFile(file);
                });
        exp.explore(root);
    }

    private void parseFile(File file) {
        try {
            // Parse the Java source file
            CompilationUnit compilationUnit = StaticJavaParser.parse(file);

            // Now you have the AST representation of your Java source file in "compilationUnit"
            // You can perform various operations on it (e.g., traverse, modify, analyze)

            ClassVisitor classVisitor = new ClassVisitor();
            classVisitor.visit(compilationUnit, null);

            EnumVisitor enumVisitor = new EnumVisitor();
            enumVisitor.visit(compilationUnit, null);

            FieldVisitor fieldVisitor = new FieldVisitor();
            fieldVisitor.visit(compilationUnit, null);
//            fieldVisitor.getAttributes().forEach(System.out::println);

            MethodVisitor methodVisitor = new MethodVisitor();
            methodVisitor.visit(compilationUnit, null);
//            methodVisitor.getMethods().forEach(System.out::println);

            List<ClassToken> classes = new ArrayList<>();

            classes.addAll(classVisitor.getClasses());
            classes.addAll(enumVisitor.getClasses());

            classes.forEach(clazz -> {
                fieldVisitor.getFields().forEach(clazz::addField);
                methodVisitor.getMethods().forEach(clazz::addMethod);

                System.out.println(clazz);
            });
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }


}
