package umlmaker.javaparser.tokens;

public class ParameterToken implements Token {
    private String name;
    private String type;
    private boolean isFinal;

    public ParameterToken(String name, String type, boolean isFinal) {
        this.name = name;
        this.type = type;
        this.isFinal = isFinal;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isFinal() {
        return isFinal;
    }

    public void setFinal(boolean aFinal) {
        isFinal = aFinal;
    }

    @Override
    public String toString() {
        return "ParameterToken{" +
                "name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", isFinal=" + isFinal +
                '}';
    }
}
