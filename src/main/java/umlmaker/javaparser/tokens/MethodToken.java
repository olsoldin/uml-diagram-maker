package umlmaker.javaparser.tokens;

import umlmaker.Tokens.Visibility;

import java.util.List;

public class MethodToken implements Token {
    private Visibility visibility;
    private String name;
    private String returnType;
    private List<ParameterToken> parameters;
    private boolean isFinal;
    private boolean isStatic;

    public MethodToken(Visibility visibility, String name, String returnType, List<ParameterToken> parameters, boolean isFinal, boolean isStatic) {
        this.visibility = visibility;
        this.name = name;
        this.returnType = returnType;
        this.parameters = parameters;
        this.isFinal = isFinal;
        this.isStatic = isStatic;
    }

    public Visibility getVisibility() {
        return visibility;
    }

    public void setVisibility(Visibility visibility) {
        this.visibility = visibility;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReturnType() {
        return returnType;
    }

    public void setReturnType(String returnType) {
        this.returnType = returnType;
    }

    public List<ParameterToken> getParameters() {
        return parameters;
    }

    public void setParameters(List<ParameterToken> parameters) {
        this.parameters = parameters;
    }

    public boolean isFinal() {
        return isFinal;
    }

    public void setFinal(boolean aFinal) {
        isFinal = aFinal;
    }

    public boolean isStatic() {
        return isStatic;
    }

    public void setStatic(boolean aStatic) {
        isStatic = aStatic;
    }

    @Override
    public String toString() {
        String prmts = "";
        for (ParameterToken param : parameters) {
            prmts += param.toString();
        }

        return "MethodToken{" +
                "visibility=" + visibility +
                ", name='" + name + '\'' +
                ", returnType='" + returnType + '\'' +
                ", parameters=" + prmts +
                ", isFinal=" + isFinal +
                ", isStatic=" + isStatic +
                '}';
    }
}
