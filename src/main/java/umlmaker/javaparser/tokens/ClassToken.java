package umlmaker.javaparser.tokens;

import umlmaker.Tokens.ClassType;
import umlmaker.Tokens.Visibility;

import java.util.ArrayList;
import java.util.List;

public class ClassToken implements Token {
    private Visibility visibility;
    private ClassType classType; // TODO: Enum this??
    private String name;
    private List<FieldToken> fields = new ArrayList<>();
    private List<MethodToken> methods = new ArrayList<>();

    public ClassToken(Visibility visibility, ClassType classType, String name) {
        this.visibility = visibility;
        this.classType = classType;
        this.name = name;
    }

    public Visibility getVisibility() {
        return visibility;
    }

    public void setVisibility(Visibility visibility) {
        this.visibility = visibility;
    }

    public ClassType getClassType() {
        return classType;
    }

    public void setClassType(ClassType classType) {
        this.classType = classType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<FieldToken> getFields() {
        return fields;
    }

    public void addField(FieldToken field) {
        this.fields.add(field);
    }

    public void setFields(List<FieldToken> fields) {
        this.fields = fields;
    }

    public List<MethodToken> getMethods() {
        return methods;
    }

    public void addMethod(MethodToken method) {
        this.methods.add(method);
    }

    public void setMethods(List<MethodToken> methods) {
        this.methods = methods;
    }

    @Override
    public String toString() {
        String flds = "";
        for (FieldToken field : fields) {
            flds += field.toString();
        }
        String mthds = "";
        for (MethodToken method : methods) {
            mthds += method.toString();
        }

        return "ClassToken{" +
                "visibility=" + visibility +
                ", classType='" + classType + '\'' +
                ", name='" + name + '\'' +
                ", fields=" + flds +
                ", methods=" + mthds +
                '}';
    }
}
