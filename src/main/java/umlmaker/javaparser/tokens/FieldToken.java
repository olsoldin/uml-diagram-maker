package umlmaker.javaparser.tokens;

import umlmaker.Tokens.Visibility;

public class FieldToken implements Token {
    private Visibility visibility;
    private String name;
    private String type;
    private String initialize;
    private boolean isFinal;
    private boolean isStatic;

    public FieldToken(Visibility visibility, String name, String type, String initialize, boolean isFinal, boolean isStatic) {
        this.visibility = visibility;
        this.name = name;
        this.type = type;
        this.initialize = initialize;
        this.isFinal = isFinal;
        this.isStatic = isStatic;
    }

    public Visibility getVisibility() {
        return visibility;
    }

    public void setVisibility(Visibility visibility) {
        this.visibility = visibility;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getInitialize() {
        return initialize;
    }

    public void setInitialize(String initialize) {
        this.initialize = initialize;
    }

    public boolean isFinal() {
        return isFinal;
    }

    public void setFinal(boolean aFinal) {
        isFinal = aFinal;
    }

    public boolean isStatic() {
        return isStatic;
    }

    public void setStatic(boolean aStatic) {
        isStatic = aStatic;
    }

    @Override
    public String toString() {
        return "FieldToken{" +
                "visibility=" + visibility +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", initialize='" + initialize + '\'' +
                ", isFinal=" + isFinal +
                ", isStatic=" + isStatic +
                '}';
    }
}
