package umlmaker.javaparser.visitor;

import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import org.checkerframework.checker.units.qual.C;
import umlmaker.Tokens.ClassType;
import umlmaker.Tokens.Visibility;
import umlmaker.javaparser.tokens.ClassToken;

import java.util.ArrayList;
import java.util.List;

public class ClassVisitor extends VoidVisitorAdapter<Void> {
    private List<ClassToken> classes = new ArrayList<>();

    @Override
    public void visit(ClassOrInterfaceDeclaration classOrInterfaceDeclaration, Void arg) {
        Visibility vis = Visibility.NONE;
        if (classOrInterfaceDeclaration.isPublic()) {
            vis = Visibility.PUBLIC;
        } else if (classOrInterfaceDeclaration.isProtected()) {
            vis = Visibility.PROTECTED;
        } else if (classOrInterfaceDeclaration.isPrivate()) {
            vis = Visibility.PRIVATE;
        }

        ClassType classType;

        if (classOrInterfaceDeclaration.isInterface()) {
            classType = ClassType.INTERFACE;
        } else {
            classType = ClassType.CLASS;
        }

        ClassToken clazz = new ClassToken(vis, classType, classOrInterfaceDeclaration.getNameAsString());
        classes.add(clazz);

        super.visit(classOrInterfaceDeclaration, arg);
    }

    public List<ClassToken> getClasses() {
        return classes;
    }
}
