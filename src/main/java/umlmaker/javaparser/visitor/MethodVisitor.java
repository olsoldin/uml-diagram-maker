package umlmaker.javaparser.visitor;

import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.body.Parameter;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import umlmaker.Tokens.*;
import umlmaker.javaparser.tokens.MethodToken;
import umlmaker.javaparser.tokens.ParameterToken;

import java.util.ArrayList;
import java.util.List;


public class MethodVisitor extends VoidVisitorAdapter<Void> {

    private List<MethodToken> methods = new ArrayList<>();

    /**
     * This method will be called for each method declaration in the Java source file
     * @param methodDeclaration
     * @param arg
     */
    @Override
    public void visit(MethodDeclaration methodDeclaration, Void arg) {
        Visibility vis = Visibility.NONE;
        if (methodDeclaration.isPublic()) {
            vis = Visibility.PUBLIC;
        } else if (methodDeclaration.isProtected()) {
            vis = Visibility.PROTECTED;
        } else if (methodDeclaration.isPrivate()) {
            vis = Visibility.PRIVATE;
        }

        NodeList<Parameter> params = methodDeclaration.getParameters();
        List<ParameterToken> paramTokens = new ArrayList<>();

        for (Parameter param : params) {
            paramTokens.add(new ParameterToken(param.getNameAsString(), param.getTypeAsString(), param.isFinal()));
        }
        MethodToken method = new MethodToken(
                vis,
                methodDeclaration.getNameAsString(),
                methodDeclaration.getTypeAsString(),
                paramTokens,
                methodDeclaration.isFinal(),
                methodDeclaration.isStatic()
        );
        methods.add(method);


        super.visit(methodDeclaration, arg);
    }

    public List<MethodToken> getMethods() {
        return this.methods;
    }
}

