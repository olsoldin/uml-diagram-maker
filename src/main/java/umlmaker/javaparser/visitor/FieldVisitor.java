package umlmaker.javaparser.visitor;

import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.FieldDeclaration;
import com.github.javaparser.ast.body.VariableDeclarator;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import umlmaker.Tokens.Argument;
import umlmaker.Tokens.Attribute;
import umlmaker.Tokens.Visibility;
import umlmaker.javaparser.tokens.FieldToken;

import java.util.ArrayList;
import java.util.List;

public class FieldVisitor extends VoidVisitorAdapter<Void> {
    private List<FieldToken> fields = new ArrayList<>();

    @Override
    public void visit(FieldDeclaration fieldDeclaration, Void arg) {
        Visibility vis = Visibility.NONE;
        if (fieldDeclaration.isPublic()) {
            vis = Visibility.PUBLIC;
        } else if (fieldDeclaration.isProtected()) {
            vis = Visibility.PROTECTED;
        } else if (fieldDeclaration.isPrivate()) {
            vis = Visibility.PRIVATE;
        }

        NodeList<VariableDeclarator> variables = fieldDeclaration.getVariables();

        for (VariableDeclarator variable : variables) {
            String init = null;
            if (variable.getInitializer().isPresent()) {
                init = variable.getInitializer().get().toString();
            }

            FieldToken field = new FieldToken(
                    vis,
                    variable.getNameAsString(),
                    variable.getTypeAsString(),
                    init,
                    fieldDeclaration.isFinal(),
                    fieldDeclaration.isStatic()
            );
            fields.add(field);
        }

        super.visit(fieldDeclaration, arg);
    }

    public List<FieldToken> getFields() {
        return this.fields;
    }
}
