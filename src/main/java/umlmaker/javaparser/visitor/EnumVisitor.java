package umlmaker.javaparser.visitor;

import com.github.javaparser.ast.body.EnumDeclaration;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import umlmaker.Tokens.ClassType;
import umlmaker.Tokens.Visibility;
import umlmaker.javaparser.tokens.ClassToken;

import java.util.ArrayList;
import java.util.List;

public class EnumVisitor extends VoidVisitorAdapter<Void> {
    private List<ClassToken> classes = new ArrayList<>();

    @Override
    public void visit(EnumDeclaration enumDeclaration, Void arg) {
        Visibility vis = Visibility.NONE;
        if (enumDeclaration.isPublic()) {
            vis = Visibility.PUBLIC;
        } else if (enumDeclaration.isProtected()) {
            vis = Visibility.PROTECTED;
        } else if (enumDeclaration.isPrivate()) {
            vis = Visibility.PRIVATE;
        }

        ClassToken clazz = new ClassToken(vis, ClassType.ENUM, enumDeclaration.getNameAsString());
        classes.add(clazz);
    }

    public List<ClassToken> getClasses() {
        return classes;
    }
}
