package umlmaker;

import umlmaker.Tokens.*;

import java.util.ArrayList;
import java.util.List;

public class ClassDataBuilder {

    private String className;
    private ClassType classType;
    private Visibility visibility;
    private List<Attribute> attributes = new ArrayList<>();
    private List<Method> methods = new ArrayList<>();

    public ClassDataBuilder() {}

    public static void testStatic() {

    }

    public final ClassDataBuilder addClassName(String className) {
        this.className = className;
        return this;
    }

    public ClassDataBuilder addClassType(ClassType classType) {
        this.classType = classType;
        return this;
    }

    public ClassDataBuilder addVisibility(Visibility visibility) {
        this.visibility = visibility;
        return this;
    }

    public final ClassDataBuilder addAttributes(List<Attribute> attributes) {
        this.attributes.addAll(attributes);
        return this;
    }

    public final ClassDataBuilder addAttribute(Attribute attribute) {
        this.attributes.add(attribute);
        return this;
    }

    public ClassDataBuilder addMethods(List<Method> methods) {
        this.methods.addAll(methods);
        return this;
    }

    public ClassDataBuilder addMethod(Method method) {
        this.methods.add(method);
        return this;
    }


    public ClassData build() {
        return new ClassData(
                className,
                classType,
                visibility,
                attributes,
                methods
        );
    }

}
