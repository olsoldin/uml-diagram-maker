package umlmaker;

import umlmaker.Tokens.*;

import java.util.List;

/**
 *
 * @author Oliver
 */
public class ClassData {

	private final String className;
	private final ClassType classType;
	private final Visibility visibility;
	private List<Attribute> attributes;
	private List<Method> methods;

	public ClassData(String className, ClassType classType, Visibility visibility, List<Attribute> attributes, List<Method> methods) {
		this.className = className;
		this.classType = classType;
		this.visibility = visibility;
		this.attributes = attributes;
		this.methods = methods;
	}

	public String getClassName() {
		return className;
	}

	public ClassType getClassType() {
		return classType;
	}

	public Visibility getVisibility() {
		return visibility;
	}

	public List<Attribute> getAttributes() {
		return attributes;
	}

	public List<Method> getMethods() {
		return methods;
	}

	private String getAttributesString() {
		StringBuilder sb = new StringBuilder("");

		for (Attribute attr : attributes) {
			sb.append(attr.toString());
			sb.append("\n");
		}

		return sb.toString();
	}

	private String getMethodsString() {
		StringBuilder sb = new StringBuilder("");

		for (Method m : methods) {
			sb.append(m.toString());
			sb.append("\n");
		}

		return sb.toString();
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(visibility == null ? "" : visibility + " ");
		sb.append(getClassType() + " ");
		sb.append(getClassName() + " ");
		sb.append("\n\n");
		sb.append(getAttributesString());
		sb.append("\n");
		sb.append(getMethodsString());

		return sb.toString();
	}

}
