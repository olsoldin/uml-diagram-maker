package umlmaker.Tokens;

/**
 *
 * @author Oliver
 */
public enum ClassType {

	CLASS("class"),
	ABSTRACT_CLASS("abstract class"),
	INTERFACE("interface"),
	ENUM("enum"),
	ANNOTATION("annotation"),
	RECORD("record");

	private final String value;
	private static int maxLength = 0;

	static{
		for(ClassType t : ClassType.values()){
			if(t.toString().length() > maxLength){
				maxLength = t.toString().length() ;
			}
		}
	}

	ClassType(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return value;
	}
	
	public static int maxLength(){
		return maxLength;
	}
}
