/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package umlmaker.Tokens;

/**
 *
 * @author Oliver
 */
public class Argument {

	private String name;
	private String type;
	private String initializer;
	private boolean isFinal;
	private boolean isStatic = false;

	/**
	 *
	 * @param name The name of the object (eg. getName / type   / isFinal)
	 * @param type The type of the object (eg. String  / String / boolean)
	 */
	public Argument(String name, String type) {
		this(name, type, false, false, null);
	}

	public Argument(String name, String type, boolean isFinal, boolean isStatic) {
		this(name, type, isFinal, isStatic, null);
	}

	public Argument(String name, String type, boolean isFinal, boolean isStatic, String initializer) {
		this.name = name;
		this.type = type;
		this.isFinal = isFinal;
		this.isStatic = isStatic;
		this.initializer = initializer;
	}

	public Argument() {}

	public String getName() {
		return name;
	}

	public String getType() {
		return type;
	}

	public boolean isFinal() {
		return isFinal;
	}

	public boolean isStatic() {
		return isStatic;
	}

	public String getInitializer() {
		return initializer;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setFinal(boolean isFinal) {
		this.isFinal = isFinal;
	}

	public void setStatic(boolean isStatic) {
		this.isStatic = isStatic;
	}

	public void setInitializer(String initializer) {
		this.initializer = initializer;
	}

	@Override
	public String toString() {
		String res = (isStatic ? "static " : "") + name + " : " + type + (isFinal ? " { final }" : "" );

		if (initializer != null) {
			res += " = " + initializer;
		}

		return res;
	}
}
