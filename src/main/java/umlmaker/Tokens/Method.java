/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package umlmaker.Tokens;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author Oliver
 */
public class Method {
	private Visibility visibility;
	private Argument methodSignature;
	private List<Argument> arguments;

	public Method(Argument methodSignature, Visibility visibility, List<Argument> arguments){
		this.methodSignature = methodSignature;
		this.visibility = visibility;
		this.arguments = arguments;
	}

	public Argument getMethodSignature() {
		return methodSignature;
	}

	public List<Argument> getArguments() {
		return arguments;
	}

	private String getArgsAsString(){
		return String.join(", ", arguments.stream().map(Argument::toString).collect(Collectors.toList()));
	}
	
	/**
	 * + toString (  ) : String
	 * @return 
	 */
	@Override
	public String toString(){
		return visibility.getSymbol() + " "
				+ (getMethodSignature().isFinal() ? "final " : "")
				+ methodSignature.getName()
				+ " ( " + getArgsAsString() + " ) : "
				+ methodSignature.getType();
	}
}
