/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package umlmaker.Tokens;

/**
 *
 * @author Oliver
 */
public enum Visibility{

	PRIVATE("private", "-"),
	PROTECTED("protected", "#"),
	PUBLIC("public", "+"),
	NONE("", "~");

	private final String value;
	private final String symbol;


	Visibility(String value, String symbol){
		this.value = value;
		this.symbol = symbol;
	}

	public String getSymbol() {
		return symbol;
	}

	@Override
	public String toString(){
		return value;
	}
}
