/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package umlmaker.Tokens;

/**
 *
 * @author Oliver
 */
public class Attribute {

	private final Argument argument;
	private final Visibility visibility;

	/**
	 * An attribute of a class (eg. this class has the attributes:
	 *	- private final Argument argument;
	 *		argument: { name: "argument", type: "Argument" }
	 *		visibility: "private"
	 *	- private final Visibility visibility;
	 *		argument: { name: "visibility", type: "Visibility" }
	 *		visibility: "private"
	 *
	 * )
	 * @param argument
	 * @param visibility 
	 */
	public Attribute(Argument argument, Visibility visibility) {
		this.argument = argument;
		this.visibility = visibility;
	}

	public Argument getArgument() {
		return argument;
	}

	public Visibility getVisibility() {
		return visibility;
	}

	@Override
	public String toString(){
		return visibility.getSymbol() + " " + argument;
	}
}
