/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package umlmaker.Tokens;

/**
 *
 * @author Oliver
 */
public enum Relationship {

	IMPLEMENTS("implements"), EXTENDS("extends"), USES("uses");

	private final String value;

	Relationship(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return value;
	}
}
